"use strict";
import logger           from 'morgan';
import cookieParser     from 'cookie-parser';
import bodyParser       from 'body-parser';
import http             from 'http'
import routeProducts    from './routes/products';
import express          from 'express'

import Db from './lib/db.js'

const app = express();
const server = http.createServer(app);

const port = process.env.PORT || 3001;

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.set("view engine","twig");
app.use(express.static('views'));

app.use('/products', routeProducts);

app.close = function() {
    server.close();
    Db.close()
}

app.listen(() => {
    server.listen(port, () => {
        console.log("Express server listening on port " + port + " in " + app.settings.env + " mode");
    });
});
