const express = require('express');
const router =  express.Router();
import Db from '../lib/db.js'

router.get('/view/:id', function(req, res) {
    Db.findOneByIdProducts(res, req.params.id)
});

router.get('/cart/:id', function(req, res) {
    Db.findOneByIdProducts(res, req.params.id, 'json')
});

router.get('/list', function(req, res) {
    Db.findAllProducts(res)
})

export default router;