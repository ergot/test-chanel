const sqlite3 = require('sqlite3').verbose();

export default new class {

    constructor(){
        this.db = new sqlite3.Database('database.sqlite');
    }

    findAllProducts(callback) {
        this.db.all("SELECT * FROM products", (err, rows) => {
            if (err) throw new Error("Database Errors");
            callback.render('list', {products: rows});
        })
    }

    findOneByIdProducts(callback, id, type = 'render') {
        this.db.get(`SELECT * FROM products WHERE id = ${id}`, function(err, row) {
            if (err) throw new Error("Database Errors");

             switch (type) {
                 case 'json':
                     callback[type]({success:true, text: `Product ${id} successfully bought`});
                     break
                 case 'render':
                     callback[type]('view', {product: row});
                     break
                 default:
                     callback.status(418).send('Tea Time')
                     break
             }
        });
    }

    close(){
        this.db.close()
    }
}


